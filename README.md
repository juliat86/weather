### About
This is a Java library that fetches weather data from a web API.

### Library
Currently the only API implemented is [OpenWeatherMap](http://openweathermap.org), however the library was implemented generically, so additinal APIs can be easily added.

### Usage Example
A simple Swing application was implemented as a usage example.

### Info
Homepage: https://bitbucket.org/juliat86/weather