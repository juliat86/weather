package DataService;

/**
 * Exception to be thrown by the service. The message can be shown in a
 * dialog by the using application.
 */
public class WeatherDataServiceException extends Exception {
    WeatherDataServiceException(String message) {
        super(message);
    }
}
