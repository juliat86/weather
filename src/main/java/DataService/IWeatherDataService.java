package DataService;

/**
 * Interface for a weather data service
 */
public interface IWeatherDataService {

    /**
     * Get weather data for a given location
     * @param location Location for which to fetch data
     * @return Weather data for the location
     * @throws WeatherDataServiceException
     */
    WeatherData getWeatherData(Location location)
        throws WeatherDataServiceException;

    /**
     * Set the units for temperature, etc.
     * @param units Units type (metric or imperial)
     */
    void setUnits(Units units);

    /**
     * Get the units for temperature, etc.
     * @return Units type (metric or imperial)
     */
    Units getUnits();

}