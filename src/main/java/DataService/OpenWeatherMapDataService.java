package DataService;

import java.io.IOException;
import java.net.*;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Fetcher for weather data from OpenWeatherMap API
 */
public class OpenWeatherMapDataService extends WeatherDataService {
    private static String API_URL = "http://api.openweathermap.org/data/2.5";
    private static String APP_ID = "d17e8b4b11d644994c8646d528e3e413";
    private static final String ICON_BASE_URL = "http://openweathermap.org/img/w/";

    /**
     * Default constructor
     */
    OpenWeatherMapDataService() {
        super();
    }

    /**
     * Constructor
     * @param updateFreqSec How frequently should the service notify its clients about an update
     *                      (in seconds). Current class updates in this frequency regardless of
     *                      whether the last data has changed.
     */
    OpenWeatherMapDataService(Integer updateFreqSec) {
        super(updateFreqSec);
    }

    /**
     * Get weather data from the OpenWeatherMap API
     * @param location Location for which to fetch data
     * @return Weather data from the API
     * @throws WeatherDataServiceException
     */
    @Override
    public WeatherData getWeatherData(Location location) throws WeatherDataServiceException {
        WeatherData weatherData = null;

        if (!location.get().matches("^[a-zA-Z'\\-, ]*$")) {
            throw new WeatherDataServiceException("Illegal location name");
        }

        try {
            String encodedLocation = URLEncoder.encode(location.get(),
                    java.nio.charset.StandardCharsets.UTF_8.toString());
            URI uri = new URI(API_URL + "/weather?q=" + encodedLocation +
                    "&appid=" + APP_ID + "&units=" + getUnitsAPIEncoding());
            System.out.println("accessing: " + uri);
            URL url = new URL(uri.toASCIIString());
            JsonNode jsonNode = getJsonNode(url);
            checkJsonNodeValidity(jsonNode);

            weatherData = new WeatherData();

            try {
                weatherData.setTemperature(jsonNode.get("main").get("temp").asDouble());
                weatherData.setUnits(getUnits());
            }
            catch (Exception ex) {
                throw new WeatherDataServiceException("Weather data did not contain temperature");
            }

            try {
                JsonNode weatherNode = jsonNode.get("weather").get(0);
                weatherData.setIconURL(new URL(ICON_BASE_URL + weatherNode.get("icon").asText() + ".png"));
                weatherData.setDescription(weatherNode.get("description").asText());
            } catch (Exception ex) {
                throw new WeatherDataServiceException("Failed to fetch current weather icon and description");
            }
        }
        catch (WeatherDataServiceException ex) {
            throw ex;
        }
        catch (Exception ex) {
            throw new WeatherDataServiceException("Error occurred: " + ex);
        }

        return weatherData;
    }

    // Private methods

    private String getUnitsAPIEncoding() throws WeatherDataServiceException {
        switch (getUnits()) {
            case METRIC:
                return "metric";
            case IMPERIAL:
                return "imperial";
            default:
                throw new WeatherDataServiceException("Unexpected units");
        }
    }

    private void checkJsonNodeValidity(JsonNode jsonNode) throws WeatherDataServiceException {
        if (jsonNode == null) {
            throw new WeatherDataServiceException("Weather API returned empty data");
        } else if (jsonNode.has("cod")) {
            Integer errorCode = jsonNode.get("cod").asInt();
            if (errorCode != 200) {
                if (jsonNode.has("message")) {
                    throw new WeatherDataServiceException(jsonNode.get("message").asText());
                } else {
                    throw new WeatherDataServiceException("Weather API returned error code " + errorCode);
                }
            }
        }
    }

    /**
     * Get API data as a JSON node. Made public only for unit testing purpose.
     * @param url API endpoint URL
     * @return JSON object returned by API
     * @throws WeatherDataServiceException
     */
    public JsonNode getJsonNode(URL url) throws WeatherDataServiceException {
        JsonNode jsonNode = null;
        ObjectMapper mapper = new ObjectMapper();
        try {
            jsonNode = mapper.readValue(url, JsonNode.class);
        }
        catch (IOException ex) {
            throw new WeatherDataServiceException("IO error occurred when reading from API");
        }
        return jsonNode;
    }
}
