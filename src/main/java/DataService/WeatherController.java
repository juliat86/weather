package DataService;

import java.util.Observable;
import java.util.Observer;

/**
 * Controller layer of the Weather library. A factory that creates a model
 * for the requested service API.
 */
public class WeatherController {
    private static final Integer UPDATE_FREQ_SEC = 0;

    /**
     * The API to be used as the data source
     */
    public static enum DataSource {
        OPEN_WEATHER_MAP
    };

    /**
     * Factory method that returns the model for the requested service API.
     * @param dataSource Enum of the data source API
     * @return The service to fetch weather data from
     * @throws WeatherDataServiceException
     */
    public WeatherDataService getWeatherDataService(DataSource dataSource)
            throws WeatherDataServiceException {

        WeatherDataService service = null;
        switch (dataSource) {
            case OPEN_WEATHER_MAP:
                service = new OpenWeatherMapDataService(UPDATE_FREQ_SEC);
                break;
            default:
                throw new WeatherDataServiceException("Unhandled DataSource");
        }

        return service;
    }
}
