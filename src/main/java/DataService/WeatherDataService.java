package DataService;

import java.util.Observable;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * Abstract base class of the weather data service that implements
 * common functionality, such as setting units and scheduling updates.
 */
public abstract class WeatherDataService extends Observable
        implements IWeatherDataService, Runnable {

    /**
     * Constructor.
     * No notification for observers.
     */
    WeatherDataService() {
        this(0);
    }

    /**
     * Constructor.
     * If the update frequency (with which the service notifies its observers) is
     * zero, then no notification occurs (the default ctor's behavior).
     * @param updateFreqSeconds
     */
    WeatherDataService(Integer updateFreqSeconds) {
        if (updateFreqSeconds > 0) {
            scheduler = Executors.newScheduledThreadPool(1);
            scheduler.scheduleAtFixedRate(this, updateFreqSeconds,
                    updateFreqSeconds, TimeUnit.SECONDS);
        }
        units = Units.METRIC;
    }

    /**
     * Set units for the service (can be changed anytime)
     * @param units Units type (metric or imperial)
     */
    @Override
    public void setUnits(Units units) {
        this.units = units;
    }

    /**
     * Get units
     * @return Units
     */
    public Units getUnits() {
        return units;
    }

    /**
     * The callback of the scheduler service. Notifies observers (with the
     * specified frequency).
     */
    @Override
    public void run() {
        setChanged();
        notifyObservers();
    }

    private ScheduledExecutorService scheduler;
    private Units units;
}
