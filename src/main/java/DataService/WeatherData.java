package DataService;

import java.net.URL;

/**
 * The weather data fetched from any of the weather services
 */
public class WeatherData {
    /**
     * Temperature
     */
    private Double temperature = null;

    /**
     * The URL of the icon that depicts the current weather conditions (e.g. cloudy)
     */
    private URL iconURL = null;

    /**
     * The textual description of the current weather conditions (e.g. intermittent showers)
     */
    private String description = null;

    /**
     * The units (metric/imperial) of the current weather data
     */
    private Units units = Units.METRIC;

    public Double getTemperature() { return temperature; }
    public void setTemperature(Double temperature) { this.temperature = temperature; }

    public URL getIconURL() { return iconURL; }
    public void setIconURL(URL iconURL) { this.iconURL = iconURL; }

    public String getDescription() { return description; }
    public void setDescription(String description) { this.description = description; }

    public Units getUnits() { return units; }
    public void setUnits(Units units) { this.units = units; }
}
