package DataService;

/**
 * Units in which the weather data (e.g. temperature) is given
 */
public enum Units {
    METRIC, IMPERIAL;

    /**
     * Convert temperature units to string
     * @return
     */
    public String temperatureUnitsString() {
        final String DEGREE  = "\u00b0";

        switch (this) {
            case METRIC:
                return String.format("%s C", DEGREE);
            case IMPERIAL:
                return String.format("%s F", DEGREE);
            default:
                return null;
        }
    }
}
