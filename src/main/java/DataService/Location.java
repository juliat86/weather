package DataService;

/**
 * Location to get weather for
 */
public class Location {

    /**
     * Constructor
     * @param name Location name such as city, region or country
     */
    public Location(String name) { mName = name; }

    /**
     * Get the location as string
     * @return
     */
    public String get() { return mName; }

    private String mName;
}
