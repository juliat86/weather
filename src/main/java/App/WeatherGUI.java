package App;

import DataService.*;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;
import java.util.LinkedList;
import java.util.Observable;
import java.util.Observer;

public class WeatherGUI implements Observer {

    // GUI Methods

    WeatherGUI() {
        toggledComponents = new LinkedList<>();
        toggledComponents.add(lblWeather);
        toggledComponents.add(lblNameWeather);
        toggledComponents.add(lblTemperature);
        toggledComponents.add(lblNameTemperature);
    }

    private void setOptionalComponentVisibility(boolean isVisible) {
        for (JComponent component : toggledComponents) {
            component.setVisible(isVisible);
        }
    }

    public void show() throws WeatherDataServiceException {
        frmMain = new JFrame("Weather");
        Border padding = BorderFactory.createEmptyBorder(10, 10, 10, 10);
        pnlMain.setBorder(padding);
        frmMain.setContentPane(pnlMain);
        frmMain.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setOptionalComponentVisibility(false);

        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (Exception ex) {}
        SwingUtilities.updateComponentTreeUI(frmMain);

        WeatherController controller = new WeatherController();
        weatherService = controller.getWeatherDataService(WeatherController.DataSource.OPEN_WEATHER_MAP);
        weatherService.addObserver(this);
        setWeatherServiceUnits();
        configureListeners();

        ImageIcon icon = new ImageIcon(getClass().getClassLoader().getResource("icons/icon.png"));
        frmMain.setIconImage(icon.getImage());

        frmMain.setMinimumSize(new Dimension(400, 0));
        frmMain.pack();
        frmMain.setLocationRelativeTo(null);
        frmMain.setVisible(true);
    }

    private void configureListeners() {
        txtLocation.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                asyncRefreshLocation();
            }
        });

        ActionListener unitsListener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                setWeatherServiceUnits();
                asyncRefreshLocation();
            }
        };

        radMetric.addActionListener(unitsListener);
        radImperial.addActionListener(unitsListener);
    }

    // Async access to weather API

    class AsyncWeatherFetcher extends SwingWorker<WeatherData, Object> {

        private final String locationName;
        private final WeatherDataService service;
        AsyncWeatherFetcher(WeatherDataService service, String location) {
            this.service = service;
            this.locationName = location;
        }

        @Override
        public WeatherData doInBackground() {
            try {
                return service.getWeatherData(new Location(locationName));
            } catch (WeatherDataServiceException ex) {
                JOptionPane.showMessageDialog(frmMain, ex.getMessage(),
                        "Error", JOptionPane.ERROR_MESSAGE);
                return null;
            }
        }

        @Override
        protected void done() {
            try{
                WeatherData weatherData = get();
                if (weatherData != null) {
                    Double temperature = weatherData.getTemperature();
                    lblTemperature.setText(temperature == null ?
                            "unknown" : String.format("%.1f%s", temperature,
                            weatherData.getUnits().temperatureUnitsString()));

                    String description = weatherData.getDescription();
                    lblWeather.setText(description == null ? "unknown" : description);
                    updateWeatherIcon(weatherData.getIconURL());
                    setOptionalComponentVisibility(true);
                    frmMain.pack();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(frmMain, "Unexpected error occurred",
                        "Error", JOptionPane.ERROR_MESSAGE);
            }
        }

    }
    private void asyncRefreshLocation()
    {
        String locationName = txtLocation.getText().trim();
        if (!locationName.isEmpty()) {
            try {
                AsyncWeatherFetcher fetcher = new AsyncWeatherFetcher(weatherService, locationName);
                fetcher.execute();
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(frmMain, "Unexpected error with async fetcher",
                        "Error", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    // Observer

    /**
     * Observer callback. Will be called when the weather service is updated.
     * @param o Unused argument (null)
     * @param arg Unused argument (null)
     */
    @Override
    public void update(Observable o, Object arg) {
        asyncRefreshLocation();
    }

    // Auxiliary methods

    private void setWeatherServiceUnits() {
        weatherService.setUnits(getUnits());
    }

    private Units getUnits() {
        if (radImperial.isSelected()) {
            return Units.IMPERIAL;
        } else {
            return Units.METRIC;
        }
    }

    private void updateWeatherIcon(URL iconUrl) {
        if (iconUrl == null) {
            lblWeather.setIcon(null);
        } else {
            try {
                BufferedImage img = ImageIO.read(iconUrl);
                ImageIcon icon = new ImageIcon(img);
                lblWeather.setIcon(icon);
            } catch (IOException ex) {
                lblWeather.setIcon(null);
                System.err.println("Couldn't load icon");
            }
        }
    }

    // Main

    public static void main(String[] args) {
        //Schedule a job for the event-dispatching thread:
        //creating and showing this application's GUI.
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                WeatherGUI gui = new WeatherGUI();

                try {
                    gui.show();
                }
                catch (WeatherDataServiceException ex) {
                    System.out.println("An error occurred");
                }
            }
        });
    }

    // Fields

    private JPanel pnlMain;
    private JFrame frmMain;
    private JTextField txtLocation;
    private JLabel lblWeather;
    private JLabel lblTemperature;
    private JLabel lblNameTemperature;
    private JLabel lblNameWeather;
    private JLabel lblNameLocation;
    private JRadioButton radMetric;
    private JRadioButton radImperial;
    private WeatherDataService weatherService;
    private LinkedList<JComponent> toggledComponents;
}
