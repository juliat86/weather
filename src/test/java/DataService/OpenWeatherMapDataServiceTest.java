package DataService;

import com.fasterxml.jackson.databind.JsonNode;
import org.hamcrest.CoreMatchers;
import org.junit.Test;

import static org.junit.Assert.*;
import org.hamcrest.Matchers;

import java.awt.image.ImagingOpException;
import java.io.IOException;
import java.net.URL;

public class OpenWeatherMapDataServiceTest {

    @Test(expected=WeatherDataServiceException.class)
    public void testGetWeatherDataFailure() throws Exception {
        OpenWeatherMapDataService weatherDataService = new OpenWeatherMapDataService();
        weatherDataService.getJsonNode(new URL("http://www.google.com"));
    }

    @Test
    public void testGetWeatherData() throws Exception {
        OpenWeatherMapDataService service = new OpenWeatherMapDataService();
        WeatherData weather = service.getWeatherData(new Location("London, UK"));
        assertThat("temperature should be double", weather.getTemperature(), Matchers.instanceOf(Double.class));

        Double temperature = weather.getTemperature();
        assertThat("temperature should be above -60", temperature, Matchers.greaterThan(-60.0));
        assertThat("temperature should be below +60", temperature, Matchers.lessThan(60.0));

        assertThat("icon url should contain http",  weather.getIconURL().toString(),
                CoreMatchers.containsString("http"));

        assertThat("weather description should be non-empty", weather.getDescription().length(),
                Matchers.greaterThan(0));

        assertThat("weather should be in the service's units", weather.getUnits(),
                Matchers.equalTo(service.getUnits()));
    }
}